import React, { Component } from 'react';
import Main from './main/index'
import './App.css';
import ButtonAppBar from "./main/AppBar";
import Favorite from "./main/Favorite";

class DashBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favMovies: [],
      onMainTab: true,
    }
  }

  render() {
    return (
      <div>
        <ButtonAppBar onMainTab={(onMainTab) => {
          this.setState({ onMainTab: onMainTab });
        }}/>
        {this.state.onMainTab ? <Main
            addFav={(fav) => {
              console.log(fav)
              this.state.favMovies.push(fav);
              this.setState({ favMovies: this.state.favMovies });
              console.log(this.state.favMovies);
              localStorage.setItem('favs', JSON.stringify(this.state.favMovies));
            }} rmFav={(fav) => {
            console.log(fav)
            this.state.favMovies = this.state.favMovies.filter(item => item.id !== fav)
            this.setState({ favMovies: this.state.favMovies });
            localStorage.setItem('favs', JSON.stringify(this.state.favMovies));
          }}
          ></Main> :
          <Favorite
            addFav={(fav) => {
              console.log(fav)
              this.state.favMovies.push(fav);
              this.setState({ favMovies: this.state.favMovies });
              localStorage.setItem('favs', JSON.stringify(this.state.favMovies));
            }} rmFav={(fav) => {
            console.log(fav)
            this.state.favMovies = this.state.favMovies.filter(item => item.id !== fav)
            this.setState({ favMovies: this.state.favMovies });
            localStorage.setItem('favs', JSON.stringify(this.state.favMovies));
          }}
          ></Favorite>}
      </div>
    );
  }
}

export default DashBoard;
