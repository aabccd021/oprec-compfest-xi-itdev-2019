import React, { Component } from 'react';
import Main from './main/index'
import './App.css';
import ButtonAppBar from "./main/AppBar";
import Favorite from "./main/Favorite";
import DashBoard from "./DashBoard";
import { Route } from "react-router-dom";
import MovieDetail from "./main/MovieDetail";
import SearchResult from "./main/SearchResult";

class App extends Component {

  render() {
    return (
      <div>
        {/*<DashBoard/>*/}
        <Route exact path="/" component={DashBoard}/>
        <Route path="/movie/:id" component={Movie}/>
        <Route path="/search/:query" component={Search}/>
      </div>
    );
  }
}

const Movie = ({ match }) => (
  <div>
    {/*<h3>ID: {match.params.id}</h3>*/}

    <MovieDetail id={match.params.id}/>
  </div>
);

const Search = ({ match }) => (
  <div>
    {/*<h3>ID: {match.params.id}</h3>*/}

    <SearchResult query={match.params.query}/>
  </div>
);

export default App;
