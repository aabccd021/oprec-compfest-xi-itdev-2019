import React, { Component } from 'react';
import SimpleCard from "./SimpleCard";
import Grid from "@material-ui/core/Grid";

class SearchResult extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movies: [],
    };
  }

  componentDidMount() {
    console.log("fetching")
    fetch('https://api.themoviedb.org/3/search/movie?api_key=6be75f67395913eaf1a30277bde07319&query='+this.props.query)
      .then(response => response.json())
      .then(data => this.setState({ movies: data.results }))
      .then(()=>console.log(this.state.movies));
  }


  render() {
    const { movies } = this.state;
    // function addFav(favMovie){
    //   console.log(favMovie)
      // this.state.favMovies.append(favMovie);
      // this.setState({favMovies:this.state.favMovies});
    // }

    return (
      <div>
        <Grid container style={{ margin: 1 }} justify={"center"}>
          {movies.map(movie => {
             return <div style={{ margin: 15 }}>
                <Grid item key={movie.id}>
                  <SimpleCard isFavorite={false} movie={movie} addFav={this.props.addFav} rmFav={this.props.rmFav}/>
                </Grid>
              </div>
            }
          )}
        </Grid>
      </div>
    );
  }

}

export default SearchResult
