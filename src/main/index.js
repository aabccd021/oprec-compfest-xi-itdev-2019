import React, { Component } from 'react';
import SimpleCard from "./SimpleCard";
import Grid from "@material-ui/core/Grid";

class Main extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movies: [],
    };
  }

  componentDidMount() {
    fetch('https://api.themoviedb.org/3/discover/movie?api_key=6be75f67395913eaf1a30277bde07319')
      .then(response => response.json())
      .then(data => this.setState({ movies: data.results }));
  }


  render() {
    const { movies } = this.state;
    // function addFav(favMovie){
    //   console.log(favMovie)
      // this.state.favMovies.append(favMovie);
      // this.setState({favMovies:this.state.favMovies});
    // }

    return (
      <div>
        <Grid container style={{ margin: 1 }} justify={"center"}>
          {movies.map(movie => {
            var moviesu = []
            var moviesutemp = JSON.parse(localStorage.getItem('favs'));
            if(moviesutemp!=null && moviesutemp!==null){
              moviesu = moviesutemp
            }
            var isFavorite = false;
            moviesu.forEach(movu=>{
              if(movu.id===movie.id){
                isFavorite = true;
              }
            });
              return <div style={{ margin: 15 }}>
                <Grid item key={movie.id}>
                  <SimpleCard isFavorite={isFavorite} movie={movie} addFav={this.props.addFav} rmFav={this.props.rmFav}/>
                </Grid>
              </div>
            }
          )}
        </Grid>
      </div>
    );
  }

}

export default Main;
