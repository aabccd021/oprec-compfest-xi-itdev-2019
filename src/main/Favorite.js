import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import SimpleCard from "./SimpleCard";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

const styles = {
    card: {
      width: 500,
      backgroundColor: '#181818',
      height: 200
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 16,
      color: '#D1D1D1',
    },
    pos: {
      marginBottom: 12,
    },
    image: {
      width: '100%'
    },
    text: {
      display: 'inline-block',
      color: '#D1D1D1',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontSize: 13,
    },
    exp: {
      margin: 20
    }
  }
;

class Favorite extends Component {

  constructor(props) {
    super(props);

    this.state = {
      movies: [],
      sort: 0,
    };
  }


  componentDidMount() {
    var moviesu = JSON.parse(localStorage.getItem('favs'));
    if (moviesu != null) {
      console.log("JJJJJJAAAAA")
      console.log(moviesu);
      this.setState({ movies: moviesu });
    }
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    if (event.target.name === "sort") {
      if (event.target.value === 1) {
        this.state.movies.sort(((a, b) => a.popularity - b.popularity))
      } else if (event.target.value === 2) {
        this.state.movies.sort(((a, b) => a.title.localeCompare(b.title)))
      } else if (event.target.value === 3) {
        this.state.movies.sort(((a, b) => a.release_date.localeCompare(b.release_date)))
      }
    }
    this.setState({movies:this.state.movies});
  };

  render() {

    const { movies } = this.state;
    return (
      <div>
        <div style={{ margin: 1 }}>
          <div>
            <Select
              value={this.state.sort}
              onChange={this.handleChange}
              inputProps={{
                name: 'sort',
                id: 'age-simple',
              }}
              style={styles.text}
            >
              <MenuItem value={0}>Recent</MenuItem>
              <MenuItem value={1}>Rating</MenuItem>
              <MenuItem value={2}>A-Z</MenuItem>
              <MenuItem value={3}>Release Date</MenuItem>
            </Select>

          </div>
          <Grid container justify={"center"}>

            {movies.map(movie =>
              <div style={{ margin: 15 }}>

                <Grid item key={movie.id}>
                  <SimpleCard isFavorite={true} movie={movie} addFav={this.props.addFav} rmFav={this.props.rmFav}/>
                </Grid>
              </div>
            )}
          </Grid>
        </div>
      </div>
    );
  }

}

export default Favorite;
