import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import Grid from "@material-ui/core/Grid";
import StarIcon from "@material-ui/icons/StarRounded"
import FavoriteIcon from "@material-ui/icons/Favorite"
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder"
import IconButton from '@material-ui/core/IconButton';

const styles = {
    card: {
      width: 500,
      backgroundColor: '#181818',
      height: 200
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 16,
      color: '#D1D1D1',
    },
    pos: {
      marginBottom: 12,
    },
    image: {
      width: '100%'
    },
    text: {
      display: 'inline-block',
      color: '#D1D1D1',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontSize: 13,
    },
    exp: {
      margin: 20
    }
  }
;


class SimpleCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isFavorite: this.props.isFavorite
    }
  }

  render() {


    const image = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/' + this.props.movie.backdrop_path;
    const title = this.props.movie.original_title;
    const overview = this.props.movie.overview;
    const id = this.props.movie.id;

    return (
      <Card style={styles.card}>
        <Grid container>
          <Grid item xs={4}>
            <a href={"movie/"+id}>
            <img src={image} alt="" style={styles.image}/>
            </a>
          </Grid>
          <Grid item xs={8}>
            <div style={styles.exp}>
              <Grid container justify="space-between">
                <a href={"movie/"+id}>
                <div style={styles.title}>
                  {title}
                </div>
                  </a>
                <IconButton style={{ margin: 0, padding: 0 }}
                            onClick={() => {
                              this.setState({ isFavorite: !this.state.isFavorite });
                              if (this.state.isFavorite) {
                                this.props.rmFav(this.props.movie.id);
                              } else {
                                this.props.addFav(this.props.movie);
                              }
                            }
                            }>
                  {this.state.isFavorite ? <FavoriteIcon style={{ color: "#666666" }}/> :
                    <FavoriteBorderIcon style={{ color: "#666666" }}/>}
                </IconButton>
              </Grid>
              <StarIcon style={{ color: "#FFA843" }}/>
              <StarIcon style={{ color: "#FFA843" }}/>
              <StarIcon style={{ color: "#FFA843" }}/>
              <StarIcon style={{ color: "#FFA843" }}/>
              <StarIcon style={{ color: "#666666" }}/>
              <p style={styles.text}>
                {overview}
              </p>
            </div>
          </Grid>
        </Grid>
      </Card>
    );
  }
}


export default SimpleCard;
