import React, { Component } from "react";
const styles = {
    card: {
      width: 500,
      backgroundColor: '#181818',
      height: 200
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 21,
      color: '#D1D1D1',
    },
    pos: {
      marginBottom: 12,
    },
    image: {
      width: '100%'
    },
    text: {
      color: '#D1D1D1',
      fontSize: 13,
    },
    exp: {
      margin: 20
    }
  }
;

class MovieDetail extends Component {
  constructor(props){
    super(props);
    this.state = {
      thedata: "",
    }
  }

  componentDidMount() {
    fetch('https://api.themoviedb.org/3/movie/'+this.props.id+'?api_key=6be75f67395913eaf1a30277bde07319')
      .then(response => response.json())
      .then(data => this.setState({ thedata: data }));
  }

  render() {

    const image = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2/' + this.state.thedata.backdrop_path;
    return(
      <div>
        <h1 style={styles.title}>
          {this.state.thedata.original_title}
        </h1>
        <h2 style={styles.title}>
          {this.state.thedata.tagline}
        </h2>
        <img src={image} alt="" style={{height:200}}/>
        <p style={styles.text}>
          {this.state.thedata.overview}
        </p>
        <br/>
        <p style={styles.text}>
          Revenue: {this.state.thedata.revenue} USD
        </p>

        <br/>
        <p style={styles.text}>
          Release Date: {this.state.thedata.release_date} USD
        </p>
      </div>
    )
  }
}

export default MovieDetail;
